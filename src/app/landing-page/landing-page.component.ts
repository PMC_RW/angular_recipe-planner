import { Component } from '@angular/core';
import { Router } from '@angular/router';

import { AuthService } from '../auth/auth.service';

@Component({
  selector: 'app-landing-page',
  templateUrl: './landing-page.component.html',
  styleUrls: ['./landing-page.component.css'],
})
export class LandingPageComponent {
  constructor(private authService: AuthService, private router: Router) {}

  onSignIn() {
    this.authService.signIn().subscribe(() => {
      if (this.authService.isSignedIn) {
        const redirectUrl = '/shopping-list';

        this.router.navigate([redirectUrl]);
      }
    });
  }
}
