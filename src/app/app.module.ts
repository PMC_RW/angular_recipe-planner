import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { HeaderComponent } from './header/header.component';
import { NavigationComponent } from './header/navigation/navigation.component';
import { LandingPageComponent } from './landing-page/landing-page.component';
import { RecipesListComponent } from './recipes/recipes-list/recipes-list.component';
import { RecipeListItemComponent } from './recipes/recipe-list-item/recipe-list-item.component';
import { RecipeDetailComponent } from './recipes/recipe-detail/recipe-detail.component';
import { ShoppingListComponent } from './shopping/shopping-list/shopping-list.component';
import { ShoppingAddComponent } from './shopping/shopping-add/shopping-add.component';

import { AuthService } from './auth/auth.service';
import { DialogService } from './shared/dialog.service';
import { RecipeEditComponent } from './recipes/recipe-edit/recipe-edit.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    NavigationComponent,
    ShoppingListComponent,
    RecipesListComponent,
    RecipeDetailComponent,
    ShoppingAddComponent,
    RecipeListItemComponent,
    LandingPageComponent,
    RecipeEditComponent,
  ],
  imports: [BrowserModule, AppRoutingModule, FormsModule],
  providers: [AuthService, DialogService],
  bootstrap: [AppComponent],
})
export class AppModule {}
