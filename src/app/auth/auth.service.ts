import { Injectable } from '@angular/core';
import { Observable, delay, of, tap } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  // set to true for easier editing
  isSignedIn: boolean = true;

  // store URL to redirect after sign in
  redirectUrl: string | null = null;

  signIn(): Observable<boolean> {
    return of(true).pipe(
      delay(800),
      tap(() => (this.isSignedIn = true))
    );
  }

  signOut(): void {
    this.isSignedIn = false;
  }
}
