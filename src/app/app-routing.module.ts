import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RecipesListComponent } from './recipes/recipes-list/recipes-list.component';
import { ShoppingListComponent } from './shopping/shopping-list/shopping-list.component';
import { RecipeDetailComponent } from './recipes/recipe-detail/recipe-detail.component';
import { LandingPageComponent } from './landing-page/landing-page.component';
import { authGuard } from './auth/auth.guard';
import { canDeactivateGuard } from './shopping/shopping-add/can-deactivate.guard';
import { ShoppingAddComponent } from './shopping/shopping-add/shopping-add.component';
import { RecipeEditComponent } from './recipes/recipe-edit/recipe-edit.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'recipes',
    pathMatch: 'full',
  },
  {
    path: 'recipes',
    component: RecipesListComponent,
    children: [
      {
        path: ':id',
        component: RecipeDetailComponent,
      },
      {
        path: ':id/edit',
        component: RecipeEditComponent,
      },
    ],
  },
  {
    path: 'shopping-list',
    canActivate: [authGuard],
    component: ShoppingListComponent,
  },
  {
    path: 'new-ingredient',
    canActivate: [authGuard],
    component: ShoppingAddComponent,
    canDeactivate: [canDeactivateGuard],
  },
  {
    path: 'landing',
    component: LandingPageComponent,
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
