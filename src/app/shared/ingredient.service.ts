import { Injectable } from '@angular/core';
import Ingredient from './ingredient.model';

@Injectable({
  providedIn: 'root',
})
export class IngredientService {
  private ingredients: Ingredient[] = [
    new Ingredient('20', 'Cherries', 20, 'pieces'),
  ];

  getAllIngredients(): Ingredient[] {
    return this.ingredients;
  }

  getOneIngredient(id: string): Ingredient {
    const [selectedIngredient] = this.ingredients.filter(
      (ingredient) => ingredient.id === id
    );
    return selectedIngredient;
  }

  addNewIngredient(newIngredient: Ingredient): void {
    this.ingredients.push(newIngredient);
  }

  addRecipeIngredients(recipeIngredients: Ingredient[]) {
    for (let ingredient of recipeIngredients) {
      this.ingredients.push(ingredient);
    }
  }
}
