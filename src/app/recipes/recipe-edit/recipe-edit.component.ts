import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';

import Recipe from '../recipe.model';
import { RecipeService } from '../recipe.service';
import { IngredientService } from 'src/app/shared/ingredient.service';

@Component({
  selector: 'app-recipe-edit',
  templateUrl: './recipe-edit.component.html',
  styleUrls: ['./recipe-edit.component.css'],
})
export class RecipeEditComponent implements OnInit {
  recipe!: Recipe;
  // implement edit recipe function, separate input from value

  constructor(
    private recipeService: RecipeService,
    private activatedRoute: ActivatedRoute
  ) {}

  ngOnInit() {
    this.activatedRoute.params.subscribe((params: Params) => {
      this.recipe = this.recipeService.getOneRecipe(params['id']);
    });
  }
}
