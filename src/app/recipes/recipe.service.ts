import { Injectable } from '@angular/core';
import Recipe from './recipe.model';
import Ingredient from '../shared/ingredient.model';

@Injectable({
  providedIn: 'root',
})
export class RecipeService {
  private recipes: Recipe[] = [
    new Recipe(
      '1',
      'Falafel',
      'Falafel is a popular Middle Eastern “fast food” made of a mixture of chickpeas (or fava beans), fresh herbs, and spices that are formed into a small patties or balls.  It’s thought that falafel originated in Egypt as Coptic Christians looked for a hearty replacement for meat during long seasons of fasting or lent.  It has also become a popular vegan food in Egypt and throughout the Middle East.',
      'https://images.unsplash.com/photo-1593001872095-7d5b3868fb1d',
      [
        new Ingredient('c1a2e3', 'dried chickpeas', 2, 'cups'),
        new Ingredient('b3d7f1', 'baking soda', 0.5, 'tsp'),
        new Ingredient('f2r4t3', 'fresh parsley leaves', 1, 'cup'),
        new Ingredient('f7e9r2', 'fresh cilantro leaves', 0.75, 'cup'),
        new Ingredient('d5i8l7', 'fresh dill', 0.5, 'cup'),
        new Ingredient('s1m2a3', 'small onion', 1, 'piece'),
        new Ingredient('g8a7r5', 'garlic cloves', 7, 'pieces'),
        new Ingredient('s4a6l7', 'salt', 0.5, 'tsp'),
        new Ingredient('b4l8a3', 'black pepper', 1, 'tbsp'),
        new Ingredient('c5o1r9', 'ground cumin', 1, 'tbsp'),
        new Ingredient('c3o6r1', 'ground coriander', 1, 'tbsp'),
        new Ingredient('c1a1y2', 'cayenne pepper', 1, 'tsp'),
        new Ingredient('b6a2k4', 'baking powder', 1, 'tsp'),
        new Ingredient('s3e8s2', 'toasted sesame seeds', 2, 'tbsp'),
        new Ingredient('o3i4l8', 'oil', 1, 'tsp'),
      ]
    ),
    new Recipe(
      '2',
      'Bananabread',
      `This easy vegan banana bread is the best ever! It’s moist, packed with flavor and comes out perfectly every time. Made with simple, easy to find ingredients, this is truly the only banana bread recipe you’ll ever need. 
      1. Preheat the oven to 350°F (180°C). Prepare a 9 x 5 inch loaf pan by spraying it with non-stick spray and lining the bottom with parchment paper. Set aside. 
      2. Sift all purpose flour into a mixing bowl and add white and brown sugar, baking powder, salt and cinnamon. Mix together.
      3.Measure the correct amount of banana and add it to your blender jug along with the soy milk and coconut oil (or vegan butter) and blend until smooth. 
      4. Pour the blended mix over your dry ingredients in the mixing bowl and mix it into a smooth batter. 
      5. Add chopped walnuts (optional) and fold in. 
      6. Transfer the batter to your prepared 9×5 loaf pan and smooth down. Add a few more chopped walnuts on top and place into the oven to bake. 
      7. Bake for 60 minutes or until a toothpick inserted into the center of the loaf comes out clean. 
      8. Let it cool in the pan for 10 minutes before removing it from the pan and placing it onto a wire cooling rack to cool completely.`,
      'https://images.unsplash.com/photo-1675712843856-ba2cb7d33f3c',
      [
        new Ingredient('a2p1f6', 'All-Purpose Flour', 16, 'tbsp'),
        new Ingredient('w1g0s0', 'White Granulated Sugar', 8, 'tbsp'),
        new Ingredient('l1b0s0', 'Light Brown Sugar', 8, 'tbsp'),
        new Ingredient('b3p0', 'Baking Powder', 3, 'tsp'),
        new Ingredient('s2a5', 'Salt', 0.25, 'tsp'),
        new Ingredient('g1c0', 'Ground Cinnamon', 1, 'tsp'),
        new Ingredient('m1r5b0', 'Mashed Ripe Banana', 1.5, 'cups'),
        new Ingredient('s5m0', 'Soy Milk (or other non-dairy milk)', 8, 'tbsp'),
        new Ingredient('c2o2', 'Coconut Oil (or Vegan Butter)', 4, 'tbsp'),
        new Ingredient('w1p0', 'Walnuts', 1, 'cup'),
      ]
    ),
  ];

  constructor() {}

  getAllRecipes(): Recipe[] {
    return this.recipes;
  }

  getOneRecipe(id: string): Recipe {
    const [selectedRecipe] = this.recipes.filter((recipe) => recipe.id === id);
    return selectedRecipe;
  }

  addNewRecipe(newRecipe: Recipe): void {
    this.recipes.push(newRecipe);
  }

  deleteOneRecipe(id: string) {
    this.recipes = this.recipes.filter((recipe) => recipe.id !== id);
  }
}
