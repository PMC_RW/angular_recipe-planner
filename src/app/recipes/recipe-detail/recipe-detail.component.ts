import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';

import Recipe from '../recipe.model';
import { RecipeService } from '../recipe.service';
import { IngredientService } from 'src/app/shared/ingredient.service';

@Component({
  selector: 'app-recipe-detail',
  templateUrl: './recipe-detail.component.html',
  styleUrls: ['./recipe-detail.component.css'],
})
export class RecipeDetailComponent implements OnInit {
  recipe!: Recipe;
  // implement listener to authService
  allowEdit: boolean = true;

  constructor(
    private recipeService: RecipeService,
    private ingredientService: IngredientService,
    private activatedRoute: ActivatedRoute
  ) {}

  ngOnInit() {
    this.activatedRoute.params.subscribe((params: Params) => {
      this.recipe = this.recipeService.getOneRecipe(params['id']);
    });
  }

  onAddIngredientsToShoppingList(id: string) {
    const recipe = this.recipeService.getOneRecipe(id);
    const ingredients = recipe.ingredients;
    this.ingredientService.addRecipeIngredients(ingredients);
    // this.router.navigate(['/shopping-list']);
  }
}
