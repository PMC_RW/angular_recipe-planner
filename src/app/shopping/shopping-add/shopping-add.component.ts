import {
  Component,
  ElementRef,
  EventEmitter,
  Output,
  ViewChild,
} from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';

import getRandomInt from 'src/app/lib/getRandomInt';

import Ingredient from 'src/app/shared/ingredient.model';
import { IngredientService } from 'src/app/shared/ingredient.service';
import { DialogService } from 'src/app/shared/dialog.service';
import { CanComponentDeactivate } from './can-deactivate.guard';

@Component({
  selector: 'app-shopping-add',
  templateUrl: './shopping-add.component.html',
  styleUrls: ['./shopping-add.component.css'],
})
export class ShoppingAddComponent implements CanComponentDeactivate {
  ingredientName: string = '';
  ingredientAmount: number | null = null;
  ingredientUnit: string = '';
  @Output() ingredientAdded = new EventEmitter();
  @ViewChild('firstInput') firstInput!: ElementRef<HTMLInputElement>;

  constructor(
    private ingredientService: IngredientService,
    private dialogService: DialogService,
    private router: Router
  ) {}

  handleAddIngredient(goBack: boolean): void {
    const ingredientId = String(getRandomInt(150, 200));
    const ingredientName = this.ingredientName;
    const ingredientAmount = Number(this.ingredientAmount);
    const ingredientUnit = this.ingredientUnit;
    const newIngredient = new Ingredient(
      ingredientId,
      ingredientName,
      ingredientAmount,
      ingredientUnit
    );
    this.ingredientService.addNewIngredient(newIngredient);
    this.ingredientAdded.emit();
    this.ingredientName = '';
    this.ingredientAmount = null;
    this.ingredientUnit = '';
    this.firstInput.nativeElement.focus();
    if (goBack) {
      this.router.navigate(['/shopping-list']);
    }
  }

  canDeactivate(): Observable<boolean> | boolean {
    if (
      this.ingredientName === '' &&
      this.ingredientAmount === null &&
      this.ingredientUnit === ''
    ) {
      return true;
    } else {
      return this.dialogService.confirm('Discard changes?');
    }
  }
}
