import { Component, OnInit } from '@angular/core';

import Ingredient from 'src/app/shared/ingredient.model';
import { IngredientService } from 'src/app/shared/ingredient.service';
import { DialogService } from 'src/app/shared/dialog.service';

@Component({
  selector: 'app-shopping-list',
  templateUrl: './shopping-list.component.html',
  styleUrls: ['./shopping-list.component.css'],
})
export class ShoppingListComponent implements OnInit {
  ingredients: Ingredient[] = [];
  ingredientName: string = '';

  constructor(
    private ingredientService: IngredientService,
    public dialogService: DialogService
  ) {}

  ngOnInit() {
    this.ingredients = this.ingredientService.getAllIngredients();
  }
}
