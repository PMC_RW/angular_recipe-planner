import { Component } from '@angular/core';
import { Router } from '@angular/router';

import { AuthService } from 'src/app/auth/auth.service';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.css'],
})
export class NavigationComponent {
  isOpen: boolean = false;

  constructor(private authService: AuthService, private router: Router) {}

  onSignOut() {
    this.authService.signOut();
    this.isOpen = false;
    this.router.navigate(['/']);
  }
}
